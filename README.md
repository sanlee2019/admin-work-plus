<img src="http://qingqingxuan.gitee.io/img/logo.png" align="center" style="zoom: 50%"/>

<h1 align = "center">Admin Work plus</h1>

## 特别感谢
首先要感谢qingqingxuan做的Admin-Work-X(基于elementui plus vue3完成的基础框架），本人在此基础上优化成后台能用的框架
为了区分出来项目包叫admin-work-plus
## 重要升级


tip: 本次升级也升级了 Element Plus 版本，由于 Elment Plus 版本和之前的差别比较大，可能在某些情况下会有 bug，如遇到请提交一个 issue，再次感谢大家的支持

## 简介：

Admin Work Plus，是 Vue Admini Work 系列中基于 Vue3 开发的中后台框架，全面的系统配置，优质模板，常用组件，真正一站式开箱即用 采用时下比较流行的 Vue3 UI 库--Element Plus

使用了当今流行的技术框架： `Vue3 + Webpack + Typescript + Element Plus`

## 项目地址：

- [🎉 官网地址](http://qingqingxuan.gitee.io/work-p-site)
- [🎉 演示地址](http://cloud.nit360.com/admin-work-plus/)
- [🎉 后台代码] https://gitee.com/sanlee2019/nit360_admin

## Admin Work Plus 联系作者

<img src="https://gitee.com/sanlee2019/img/raw/master/%E4%B8%AA%E4%BA%BA%E8%81%94%E7%B3%BB.png" style="zoom:50%;" /> 


## admin-work-plus 前端讨论群及 QQ 客服

| <img src="http://qingqingxuan.gitee.io/img/qq-custom.png" style="zoom:20%;" /> | <img src="http://qingqingxuan.gitee.io/img/qq-vip-group.png" style="zoom:20%;" /> |
| :----------------------------------------------------------------------------: | :-------------------------------------------------------------------------------: |


## 适合人群

- 正在以及想使用 vue/element plus 开发，最好是有一定的编程知识，或者原来只从事切图写静态页的人想提高自己的前端能力。
- 熟悉 Vue.js 技术栈，使用它开发过几个实际项目。
- 对原理技术感兴趣，想进阶和提升的同学。
- 特别适合全栈开发，低成本开发，将传统开发成本降低；
- 适合快速构建自己的信息化系统及平台，适合接单，适合快速出原型

## 下载&安装

```shell
# 克隆项目，使用gitee地址
git clone https://gitee.com/sanlee2019/admin-work-plus.git

# 进入项目目录
cd admin-work-plus
# 安装依赖
npm install
# 本地开发 启动项目
npm run dev
```



| **🚀 [vue3.x](https://cn.vuejs.org/)**                     |
| ---------------------------------------------------------- |
| **[🚀 element-plus]([网站快速成型工具)**                   |
| **[🚀 vue-router-next](https://next.router.vuejs.org/)**   |
| **[🚀 vuex-next](https://next.vuex.vuejs.org/)**           |
| **[🚀 axios](http://www.axios-js.com/)**                   |
| **[🚀 mockjs](http://mockjs.com/)**                        |
| **[🚀 echarts](https://echarts.apache.org/zh/index.html)** |

## 优势及注意事项

```tex
admin-work-plus 有如下优势:
1. 支持前端控制路由权限和后端控制路由模式
2. 与后台无缝对接
3. 提供了非常多的 mxin 代码块，方便集成各种功能
4. 内置了大量常用的组件，比如，上传，消息提示等
5. 支持多主题、多布局切换
6. 将前后开发人员的沟通降到“零”，让后台开发人员可做部分前端面开发，让前端人员关注做个性化开发
7. 前端开发的东西可以模板化后，我这个系统会越来越强大，全自动生成的代码，代码可读性高，代码风格一致，可维护高，支持二次开发
8. 支持多表级联查询，复杂字段任何组合查询，做数据分析及决策的智能小助手
9.
10. 丰富多样的主题，个性化表格展示
使用注意事项:
1. 项目默认使用使用vscode工具进行开发，也是唯一推荐的开发工具
2. 项目默认eslint校验规范
3. 项目仅供个人或者团队学习商用。
```

功能说明：
权限已经基本完成，菜单自动路由，每次自动生成代码后要去角色管理去进行权限分配，然后重新登录就可以看到新菜单，按钮权限需用到后到的
接口，前端对获取的权限标识缓存起来再结合v-if/v-show来判断达到按钮权限的控制，支持MD5、token
1、菜单管理：批量删除、新增、修改、查询
2、角色管理：批量删除、新增、修改、查询、权限分配（支持菜单、按钮）
3、用户角色：批量删除、新增、修改、查询
4、用户管理：批量删除、新增、修改、查询
5、用户权限：查询
6、数据字典：批量删除、新增、修改、查询
7、系统配置：批量删除、新增、修改、查询
8、查询条件：批量删除、新增、修改、查询
9、登录、动态菜单路由
10、部门管理：批量删除、新增、修改、查询
11、自动生成：
1）菜单
2）查询列表（对就菜单）、增、删、查、改
3）表单自动适配生成select、textarea、input、date表单元素
4）表单查询条件配置会自动适配生成select、input、date表单元素

## 效果图

| <img src="https://gitee.com/sanlee2019/img/raw/master/p1.png" style="zoom:20%;" /> | <img src="https://gitee.com/sanlee2019/img/raw/master/p2.png" style="zoom:20%;" />        |
| :---------------------------------------------------------------------------: | ------------------------------------------------------------------------------------ |
| <img src="https://gitee.com/sanlee2019/img/raw/master/p3.png" style="zoom:20%;" /> | <img src="https://gitee.com/sanlee2019/img/raw/master/p4.png" style="zoom:20%;" />        |
| <img src="https://gitee.com/sanlee2019/img/raw/master/p5.png" style="zoom:20%;" /> | <img src="https://gitee.com/sanlee2019/img/raw/master/p6.png" style="zoom:20%;" />        |
| <img src="https://gitee.com/sanlee2019/img/raw/master/p7.png" style="zoom:20%;" /> | <img src="https://gitee.com/sanlee2019/img/raw/master/p8.png" style="zoom:20%;" /> |
| <img src="https://gitee.com/sanlee2019/img/raw/master/p9.png" style="zoom:20%;" /> 
