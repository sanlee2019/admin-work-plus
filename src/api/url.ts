export const baseAddress = "http://work.nit360.com/";
// export const baseAddress = "http://127.0.0.1:8788/";

export const test = "/test";

export const login = "/login";

export const updateUserInfo = "/updateUser";

export const addUserInfo = "/addUser";

//测试
export const getMenuListByRoleId = "/system/findMenuByRoleOid" ;
//"getMenuListByRoleId";

export const findMenuByUserOid = "/system/findMenuByUserOid" ;

export const getAllMenuByRoleId = "/getAllMenuByRoleId";

export const deleteUserById = "/deleteUserById";

export const getDepartmentList = "/system/findDepartmentByParam";

export const addDepartment = "/addDepartment";

export const getRoleList = "/getRoleList";

export const getMenuList = "/getMenuList";

export const getParentMenuList = "/getParentMenuList";

export const getTableList = "/getTableList";

export const getCardList = "/getCardList";

export const getCommentList = "/getCommentList";

//查询条件
export const getQueryData = '/system/findSearchConditionByParam'

//部门列表
export const queryDepartmentList = '/system/queryDepartmentList'
//部门列表
export const findDepartmentList = '/system/findDepartmentByParam'
//部门新增提交
export const saveDepartment = '/system/saveDepartment'
//部门修改提交
export const updateDepartment = '/system/updateDepartment'
//删除部门列表
export const deleteBatchDepartment = '/system/deleteBatchDepartment'
//删除部门
export const deleteDepartment = '/system/deleteDepartment'
//login
export const checkLogin = "/system/login";

export const checkPerms = "/system/findUserAccessFunction";

//findMenu
export const findMenu = "/system/findMenuByParam";

//角色列表
export const queryRoleList = '/system/queryRoleList'
//所有角色列表
export const findRole = '/system/findRoleByParam'
//角色新增提交
export const saveRole = '/system/saveRole'
//角色修改提交
export const updateRole = '/system/updateRole'
//删除车辆列表
export const deleteBatchRole = '/system/deleteBatchRole'

export const queryMenuList = '/system/queryMenuList'
export const saveMenu = '/system/saveMenu'
export const updateMenu = '/system/updateMenu'
export const deleteBatchMenu = '/system/deleteBatchMenu'
export const deleteMenu = '/system/deleteMenu'

export const queryUserList = '/system/queryUserList'
export const saveUser = '/system/saveUser'
export const updateUser = '/system/updateUser'
export const deleteBatchUser = '/system/deleteBatchUser'

export const queryUserRoleList = '/system/queryUserRoleList'
export const saveUserRole = '/system/saveUserRole'
export const updateUserRole = '/system/updateUserRole'
export const deleteBatchUserRole = '/system/deleteBatchUserRole'

export const queryUserRightList = '/system/queryUserRightList'
export const saveUserRight = '/system/saveUserRight'
export const updateUserRight = '/system/updateUserRight'
export const deleteBatchUserRight = '/system/deleteBatchUserRight'

export const querySearchConditionList = '/system/querySearchConditionList'
export const saveSearchCondition = '/system/saveSearchCondition'
export const updateSearchCondition = '/system/updateSearchCondition'
export const deleteBatchSearchCondition = '/system/deleteBatchSearchCondition'

export const queryFsDictList = '/system/queryFsDictList'
export const saveFsDict = '/system/saveFsDict'
export const updateFsDict = '/system/updateFsDict'
export const deleteBatchFsDict = '/system/deleteBatchFsDict'

export const queryAppConfigList = '/system/queryAppConfigList'
export const saveAppConfig = '/system/saveAppConfig'
export const updateAppConfig = '/system/updateAppConfig'
export const deleteBatchAppConfig = '/system/deleteBatchAppConfig'

export const queryOperationLogList = '/system/queryOperationLogList'
export const saveOperationLog = '/system/saveOperationLog'
export const updateOperationLog = '/system/updateOperationLog'
export const deleteBatchOperationLog = '/system/deleteBatchOperationLog'

export const queryAccessControlList = '/system/queryAccessControlList'
export const saveAccessControl = '/system/saveAccessControl'
export const updateAccessControl = '/system/updateAccessControl'
export const deleteBatchAccessControl = '/system/deleteBatchAccessControl'

export const queryNoticeCategoryList = '/system/queryNoticeCategoryList'
export const saveNoticeCategory = '/system/saveNoticeCategory'
export const updateNoticeCategory = '/system/updateNoticeCategory'
export const deleteBatchNoticeCategory = '/system/deleteBatchNoticeCategory'

export const queryNoticeList = '/system/queryNoticeList'
export const saveNotice = '/system/saveNotice'
export const updateNotice = '/system/updateNotice'
export const deleteBatchNotice = '/system/deleteBatchNotice'

export const queryWorkOrderList = '/system/queryWorkOrderList'
export const saveWorkOrder = '/system/saveWorkOrder'
export const updateWorkOrder = '/system/updateWorkOrder'
export const deleteBatchWorkOrder = '/system/deleteBatchWorkOrder'

export const queryEmergenceGroupList = '/system/queryEmergenceGroupList'
export const saveEmergenceGroup = '/system/saveEmergenceGroup'
export const updateEmergenceGroup = '/system/updateEmergenceGroup'
export const deleteBatchEmergenceGroup = '/system/deleteBatchEmergenceGroup'


