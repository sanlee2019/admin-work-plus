import Axios, { AxiosResponse } from "axios";
import qs from "qs";

// const baseIp = "http://10.168.170.59:8788";

// const baseIp = "http://127.0.0.1:8788";

const baseIp = "http://work.nit360.com/";

export const CONTENT_TYPE = "Content-Type";

export const FORM_URLENCODED =
  "application/x-www-form-urlencoded; charset=UTF-8";

export const APPLICATION_JSON = "application/json; charset=UTF-8";

export const TEXT_PLAIN = "text/plain; charset=UTF-8";

export const ACCESS_TOKEN = "access-Token";

export const ACCESS_TOKEN_KEY = "x-auth-token";

const service = Axios.create({
  baseURL: baseIp,
  timeout: 10 * 60 * 1000,
  withCredentials: true, // 跨域请求时发送cookie
});

service.interceptors.request.use(
  (config) => {
    !config.headers && (config.headers = {});
    config.headers["Admin-Work-Version"] = "plus";
    if (!config.headers[CONTENT_TYPE]) {
      config.headers[CONTENT_TYPE] = APPLICATION_JSON;
    }
    if (config.headers[CONTENT_TYPE] === FORM_URLENCODED) {
      config.data = qs.stringify(config.data);
    }
    if(localStorage.getItem(ACCESS_TOKEN) !== null){
      config.headers[ACCESS_TOKEN_KEY] = localStorage.getItem(ACCESS_TOKEN);
    }
    return config;
  },
  (error) => {
    return Promise.reject(error.response);
  }
);

service.interceptors.response.use(
  (response: AxiosResponse): AxiosResponse => {
    if (response.status === 200) {
      return response;
    } else {
      throw new Error(response.status.toString());
    }
  },
  (error) => {
    if (process.env.NODE_ENV === "development") {
      console.log(error);
    }
    return Promise.reject({ code: 500, msg: "服务器异常，请稍后重试…" });
  }
);

export default service;
