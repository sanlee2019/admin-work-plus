/* eslint-disable */
export function testClassDeco(constructor: Function) {
  console.log(constructor);
  console.log(constructor.prototype);
}


export function uuid() {
  const s: Array<any> = []
  const hexDigits = '0123456789abcdef'
  for (let i = 0; i < 36; i++) {
    s[i] = hexDigits.substr(Math.floor(Math.random() * 0x10), 1)
  }
  s[14] = '4' // bits 12-15 of the time_hi_and_version field to 0010
  s[19] = hexDigits.substr((s[19] & 0x3) | 0x8, 1) // bits 6-7 of the clock_seq_hi_and_reserved to 01
  s[8] = s[13] = s[18] = s[23] = '-'
  const uuid = s.join('')
  return uuid
}

export function randomString(length: number) {
  var str = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'
  var result = ''
  for (var i = length; i > 0; --i) { result += str[Math.floor(Math.random() * str.length)] }
  return result
}

/**
 * 中划线字符驼峰
 * @param {*} str 要转换的字符串
 * @returns 返回值
 */
 export function toHump(str: string) {
  if (!str) return str
  return str.replace(/\-(\w)/g, function (all, letter) {
    return letter.toUpperCase()
  }).replace(/(\s|^)[a-z]/g, function (char) {
    return char.toUpperCase()
  })
}

export function currentDate() {
  const d = new Date()
  let str = ''
  str += d.getFullYear() + '-' // 获取当前年份
  const month = d.getMonth() + 1
  str += month < 10 ? '0' + month + '-' : month + '-' // 获取当前月份（0——11）
  str += d.getDate() + ' '
  const hours = d.getHours()
  str += hours < 10 ? '0' + hours : hours + ':'
  str += d.getMinutes() + ':'
  str += d.getSeconds()
  return str
}

//数组转化为树
export  function arraytotree(arr: any) {
    var top: any = [], sub: any = [], tempObj: any = {};
    arr.forEach((item: any) => {
      item.id = item.menuOid;
      item.parentId = item.parentOid;
        if (item.parentOid === 0) { // 顶级分类
            top.push(item)
        } else {
            sub.push(item) // 其他分类
        }
        item.children = []; // 默然添加children属性
        tempObj[item.menuOid] = item // 用当前分类的id做key，存储在tempObj中
    })
    sub.forEach((item: any) =>{
        if(item.menuType !== 2){ //将按钮菜单过滤掉
          // 取父级
          var parent = tempObj[item.parentOid] || {'children': []}
          // 把当前分类加入到父级的children中
          parent.children.push(item)
       }
    })
    return top;
}

//数组转化为树
export  function allMenuToTree(arr: any) {
  var top: any = [], sub: any = [], tempObj: any = {};
  arr.forEach((item: any) => {
    item.id = item.menuOid;
    item.parentId = item.parentOid;
      if (item.parentOid === 0) { // 顶级分类
          top.push(item)
      } else {
          sub.push(item) // 其他分类
      }
      item.children = []; // 默然添加children属性
      tempObj[item.menuOid] = item // 用当前分类的id做key，存储在tempObj中
  })
  sub.forEach((item: any) =>{
      // 取父级
      var parent = tempObj[item.parentOid] || {'children': []}
      // 把当前分类加入到父级的children中
      parent.children.push(item)
  })
  return top;
}

//数组转化为树
export  function depArraytotree(arr: any) {
  var top: any = [], sub: any = [], tempObj: any = {};
  arr.forEach((item: any) => {
      item.id = item.departmentOid;
      item.parentId = item.parentOid;
      if (item.parentId === 0) { // 顶级分类
          top.push(item)
      } else {
          sub.push(item) // 其他分类
      }
      item.children = []; // 默然添加children属性
      tempObj[item.departmentOid] = item // 用当前分类的id做key，存储在tempObj中
  })
  sub.forEach((item: any) =>{
      // 取父级
      var parent = tempObj[item.parentId] || {'children': []}
      // 把当前分类加入到父级的children中
      parent.children.push(item)
  })
  return top;
}


  export function isShowed(row:any) :boolean{
    let accessRight = localStorage.getItem("perm");
    let isAccessRight: any;
    if(accessRight !== undefined && accessRight?.includes(row)){
      isAccessRight = true;
    }else{
      isAccessRight = false;
    }
    return isAccessRight
  }